# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/xb/sonysdk-arm64/app/CameraDevice.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/CameraDevice.cpp.o"
  "/home/xb/sonysdk-arm64/app/CameraManager.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/CameraManager.cpp.o"
  "/home/xb/sonysdk-arm64/app/ConnectionInfo.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/ConnectionInfo.cpp.o"
  "/home/xb/sonysdk-arm64/app/EventEmitter.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/EventEmitter.cpp.o"
  "/home/xb/sonysdk-arm64/app/LiveViewStreamThread.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/LiveViewStreamThread.cpp.o"
  "/home/xb/sonysdk-arm64/app/MessageDefine.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/MessageDefine.cpp.o"
  "/home/xb/sonysdk-arm64/app/PropertyValueTable.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/PropertyValueTable.cpp.o"
  "/home/xb/sonysdk-arm64/app/RemoteCli.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/RemoteCli.cpp.o"
  "/home/xb/sonysdk-arm64/app/SonyDeviceChecked.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/SonyDeviceChecked.cpp.o"
  "/home/xb/sonysdk-arm64/app/Text.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/Text.cpp.o"
  "/home/xb/sonysdk-arm64/app/WebsocketServer.cpp" "/home/xb/sonysdk-arm64/build/CMakeFiles/RemoteCli.dir/app/WebsocketServer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../app/CRSDK"
  "/usr/include/gstreamer-1.0"
  "/usr/include/glib-2.0"
  "/usr/lib/arm-linux-gnueabihf/glib-2.0/include"
  "/usr/include/arm-linux-gnueabihf"
  "/usr/local/Cellar/libffi/3.4.2/include"
  "/usr/local/Cellar/gstreamer/1.20.3/include/gstreamer-1.0"
  "/usr/local/Cellar/glib/2.72.3/include"
  "/usr/local/Cellar/glib/2.72.3/include/glib-2.0"
  "/usr/local/Cellar/glib/2.72.3/lib/glib-2.0/include"
  "/usr/local/opt/gettext/include"
  "/usr/local/Cellar/pcre/8.45/include"
  "/usr/local/include/opencv4/opencv2"
  "/usr/include/opencv4/opencv2"
  "../app/websocketpp"
  "/usr/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
